# Projet Cartographie

L'objectif est de programmer une application de cartographie permettant
d'exploiter des données ouvertes « open data ».

Ces données seront par exemple
[celles proposées](https://donnees.grandchambery.fr/) par la ville de Chambéry
, ou celle [d'Etablab](https://geo.api.gouv.fr/) :

Un autre jeu de données, en open data, de type géographique, et possédant une
API REST pourra être utilisé.

*Durée :* 2 semaines, environ 40 heures (10 demi-journées).

*Groupe *: projet à réaliser en binôme

## Objectifs

-   Conception d'une application sur la base d'un scénario utilisateur
-   Approfondissement des apprentissages JavaScript
    - asynchronicité via l'utilisation de l'API « Fetch », des Promise
    - stockage des données localement par l'API Web Storage
-   Découverte de l'« open data » et de l'utilisation de services Web
    exploitant une architecture REST
-   Prise en main de l'outil WebPack entre autres pour améliorer l'organisation
    du code source Javascript par l'intermédiaire de l'instruction « import »
-   Découverte de « OpenStreetMap » et de son utilisation par l'intermédiaire
    de la bibliothèque de carte interactive Leaflet
-   Découverte de l'open source, des enjeux autour des licences logicielles

## Compétences

-   Front-end : Maquetter une application
-   Front-end : Réaliser une interface utilisateur Web statique et
-   adaptable aux différents périphériques
-   Front-end : Développer une interface utilisateur Web dynamique

## Contraintes

-   L'ensemble du code source (nom des variables, fonctions) et des
    commentaires doivent être en anglais.
-   Les données « open data » doivent obligatoirement être exploitées
    via les services web REST par l'intermédiaire de Fetch.
-   L'interface doit être pensée pour le mobile en premier, et adaptable aux
    différents terminaux et tailles d'écran (mobiles, tablettes, ordinateurs).
*   L'outil WebPack doit être employé.

## Projet

Vous devrez analyser et sélectionner un ou plusieurs jeux de données en open
data, contenant des éléments géographiques.

Vous devrez concevoir une maquette graphique et un scénario utilisateur
permettant d'exploiter les données précédemment sélectionnées. Ne pas oublier
les contraintes citées au-dessus.

L'objectif principal est de récupérer les données via REST et l'API Fetch de
Javascript. Ces données devront être affichées sur une carte interactive par
l'intermédiaire de la bibliothèque Javascript Leaflet. Afin de simplifier
l'organisation de votre code Javascript, il est demandé d'exploiter l'outil
WebPack et à minima l'instruction « import » introduite avec l'arrivée de
ECMAScript 6.

### Bonus

-   Alimenter l'application avec des informations complémentaires en exploitant
    les données importées via le projet "base de données" (export json)
-   Transpiler son code ES6/7 en ES5 pour s'assurer de son exécution sur
    les anciens navigateurs
-   Optimiser sa configuration WebPack pour une prise en charge de la
    minification, watch, gestion d'une configuration pour un
    environnement de dev ou de prod
*   Mettre tout ou partie des données reçues en REST en cache par le biais de
    l'API Web Storage voire d'une base de donnes NoSQL dans le navigateur
