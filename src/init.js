import {createSpinner, removeSpinner} from './spinner.js';
import {filterApi} from './filterApi.js';
import { formDisplay } from './formDisplay.js';
export  async function init(){
    formDisplay();
    let map = L.map('map').setView([49,3], 5);
    
    L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png').addTo(map);
    document.getElementById("region").addEventListener("change",formDisplay);
    document.getElementById("Button").addEventListener("click",  async function(){

        createSpinner();

        var reg = document.getElementById("region").value;
        var dept = document.getElementById("depart").value;
        
        let returnArr = {region:reg, departement:dept};
        if(reg==="" && dept===""){
            alert("Filtrez votre recherche");
        }
            
            
        filterApi(returnArr,map);
            
    });
}


  
 