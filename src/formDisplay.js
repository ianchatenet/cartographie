export function formDisplay(){
    
    let Auvergne_Rhone_Alpes=["Ain","Allier","Ardèche","Cantal","Drôme","Haute-Loire","Isère","Loire","Puy-de-Dôme","Rhône","Savoie"];
    

    let Bourgogne_Franche_Comte=["Côte-d'Or","Doubs","Haute-Saône","Jura","Nièvre","Saône-et-Loire","Territoire de Belfort","Yonne"]
    let Bretagne=["Côtes-d'Armor","Finistère","Ille-et-Vilaine","Morbihan"];
       

    let Centre_Val_de_Loire=["Cher","Eure-et-Loir","Indre","Indre-et-Loire","Loir-et-Cher","Loiret"];

    let Corse=["Corse-du-Sud","Haute-Corse"];
        
    let Grand_Est=["Ardennes","Aube","Bas-Rhin","Haut-Rhin","Haute-Marne","Marne","Meurthe-et-Moselle","Meuse","Moselle","Vosges"];
       
    let Haut_de_France=["Aisne","Nord","Oise","Pas-de-Calais","Somme"];

    let Ile_de_France=["Essonne","Hauts-de-Seine","Paris","Seine-Saint-Denis","Val-d'Oise","Val-de-Marne","Yvelines"];
    
    let Normandie=["Calvados","Eure","Manche","Orne","Seine-Maritime"];
    
    let Nouvelle_Aquitaine=["Charente","Charente-Maritime","Corrèze","Creuse","Deux-Sèvres","Dordogne","Gironde","Haute-Vienne","Landes",
    "Lot-et-Garonne","Pyrénées-Atlantiques","Vienne"];
    
    let Occitanie=["Ariège","Aude","Aveyron","Gard","Gers","Haute-Garonne","Hautes-Pyrénées","Hérault","Lot",
    "Lozère","Pyrénées-Orientales","Tarn","Tarn-et-Garonne"];
    
    let Pays_de_la_Loire=["Loire-Atlantique","Maine-et-Loire","Mayenne","Sarthe","Vendée"];
    
    let Provence_Alpes_Cote_Azur=["Alpes-de-Haute-Provence","Alpes-Maritimes","Bouches","Hautes-Alpes","Var","Vaucluse"]

    let truc = document.getElementById("region").value;
    let opt = "";

    switch(truc){
        case 'Auvergne-Rhones-Alpes':
            document.getElementById("depart").innerHTML=""
             Auvergne_Rhone_Alpes.forEach(element => {
                opt =`<option value="${element}">${element}</option>`;
                document.getElementById("depart").innerHTML += opt;
            });
            
        break;

        case 'Bourgogne-Franche-Comté':
            document.getElementById("depart").innerHTML=""
            Bourgogne_Franche_Comte.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case 'Bretagne':
            document.getElementById("depart").innerHTML=""
            Bretagne.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case 'Centre-Val-de-Loire':
            document.getElementById("depart").innerHTML=""
            Centre_Val_de_Loire.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;
        
        case 'Corse':
            document.getElementById("depart").innerHTML=""
            Corse.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case 'Grand Est':
            document.getElementById("depart").innerHTML=""
            Grand_Est.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case 'Hauts-de-France':
            document.getElementById("depart").innerHTML=""
            Haut_de_France.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case 'Île-de-France':
            document.getElementById("depart").innerHTML=""
            Ile_de_France.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case 'Normandie':
            document.getElementById("depart").innerHTML=""
            Normandie.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case 'Nouvelle-Aquitaine':
            document.getElementById("depart").innerHTML=""
            Nouvelle_Aquitaine.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case 'Occitanie':
            document.getElementById("depart").innerHTML=""
            Occitanie.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case 'Pays de la Loire':
            document.getElementById("depart").innerHTML=""
            Pays_de_la_Loire.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case "Provence-Alpes-Côte d'Azur":
            document.getElementById("depart").innerHTML="";
            Provence_Alpes_Cote_Azur.forEach(element => {
               opt =`<option value="${element}">${element}</option>`;
               document.getElementById("depart").innerHTML += opt;
            });
        break;

        case "Guadeloupe":
            document.getElementById("depart").innerHTML="";
            opt =`<option value=Guadeloupe>Guadeloupe</option>`;
               document.getElementById("depart").innerHTML += opt;
        break;

        case "Martinique":
            document.getElementById("depart").innerHTML="";
            opt =`<option value=Martinique>Martinique</option>`;
               document.getElementById("depart").innerHTML += opt;
        break;
        case "Guyane":
            document.getElementById("depart").innerHTML="";
            opt =`<option value=Guyane>Guyane</option>`;
               document.getElementById("depart").innerHTML += opt;
        break;
        
        case "La Réunion":
            document.getElementById("depart").innerHTML="";
            opt =`<option value="La Réunion">La Réunion</option>`;
               document.getElementById("depart").innerHTML += opt;
        break;

        case "Mayotte":
            document.getElementById("depart").innerHTML="";
            opt =`<option value=Mayotte>Mayotte</option>`;
               document.getElementById("depart").innerHTML += opt;
        break;

        default:
    }
    
}



