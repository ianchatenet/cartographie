import {createSpinner, removeSpinner} from './spinner.js';
import {createMarkers} from './createmap.js';

export  async function filterApi(array,map) {
    
    let reg = array.region;
    let dept =  array.departement;
    let returnArray=[];
    let paramString = "https://data.culture.gouv.fr/api/records/1.0/search/?";    
    let getEltNmb = new URLSearchParams();

    let url=setUrl(getEltNmb,reg,dept,paramString);
    
       await fetch(url).then(function(response){   
            response.json().then( async function(json){
                let elmtNmb = json.nhits;
                let returnValues = await searchParams(elmtNmb,reg,dept);
                returnArray.push(returnValues);
                
                createMarkers(returnArray,map,reg);
            })
        })    
}

function setUrl(getEltNmb,reg,dept,paramString){

    getEltNmb.set("dataset","liste-des-immeubles-proteges-au-titre-des-monuments-historiques");

    if(reg){
        getEltNmb.set("refine.region",reg);
    }
    
    if(dept){
        getEltNmb.set("refine.dpt_lettre",dept);
    }
    
    let paramNmb = getEltNmb.toString();
    let url = paramString+paramNmb;
    return url;
}




 async function searchParams(nbr,reg,dept){
    let returnArray=[];
    let paramString = "https://data.culture.gouv.fr/api/records/1.0/search/?";
    let startNbr = 0;
    let rowsNbr = 1000;
    let tmpNbr = nbr;
    let loopNbr;

    do{
        let searchParams = new URLSearchParams();
        searchParams.set("dataset","liste-des-immeubles-proteges-au-titre-des-monuments-historiques");
        
        if(reg){
            searchParams.set("refine.region",reg);
        }
        
        if(dept){
            searchParams.set("refine.dpt_lettre",dept)
        }
       
        searchParams.set("rows",rowsNbr);

        searchParams.set("start",startNbr)
        
        
        let param = searchParams.toString();

        let url = paramString+param;
       await fetch(url).then(function(response){
            response.json().then(function(json){
                if(tmpNbr>=1000){
                    
                    loopNbr=1000;
                }else{
                    
                    loopNbr=tmpNbr;
                }
                for(let i=0;i<loopNbr;i++){
                        
                        let test = json.records[i].hasOwnProperty("geometry");

                        if(test===false){
                            continue;
                        }
                        
                        let hist= json.records[i].fields.hasOwnProperty("hist");
                        let tmpDesc;
                        
                        if(hist===true){
                            tmpDesc = json.records[i].fields.hist;
                             
                        }else{
                            tmpDesc= "Non renseigné";
                        }

                        let tmpCom = json.records[i].fields.commune ;
                        let tmpLat = json.records[i].geometry.coordinates[1];
                        let tmpLng = json.records[i].geometry.coordinates[0];
                        let tmpTitre=json.records[i].fields.tico;
                        let obj={
                            Lat:tmpLat,
                            Lng:tmpLng,
                            Com:tmpCom,
                            Titre:tmpTitre,
                            Desc:tmpDesc
                        }
                        returnArray.push(obj);
                        startNbr += 1;
                        tmpNbr -=1;
                }
            })
        })
    }while(startNbr<nbr)
    
return returnArray;    
}

