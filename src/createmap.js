import { featureGroup, geoJSON, bounds } from "leaflet";
import {removeSpinner} from "./spinner.js";


export function createMarkers(array,map,reg){
    let myLayer=L.layerGroup();
    let cluster;
    let bunds;
if (reg == "Guadeloupe" || reg == "Martinique" || reg == "Guyane" || reg == "La Réunion" || reg == "Mayotte") {
    let northWest = L.latLng(85, -170);
    let southEast = L.latLng(-84, 191);
    bunds = L.latLngBounds(southEast, northWest);
} else {
    let northWest = L.latLng(51.3, -5.7);
    let southEast = L.latLng(41.1, 10);
    bunds = L.latLngBounds(southEast, northWest);
}

   refreshMap(myLayer,cluster);
    
    let greenIcon = L.icon({
        iconUrl: 'Assets/icon.svg',
        iconSize: [20, 20], 
        iconAnchor: [16, 37], 
        popupAnchor: [0, -30], 
    });
    
     cluster = L.markerClusterGroup({
        iconCreateFunction: function(cluster){
            return L.divIcon({
                html:cluster.getChildCount(),
                className: 'clusterStyle',
                iconSize: null
            });
        }
    });

        setMarkers(array,greenIcon,cluster,bunds);

        myLayer.addLayer(cluster);
        let aa=cluster.getBounds();
        myLayer.addTo(map);
        map.fitBounds(aa);

        removeSpinner();
    }

    function refreshMap(myLayer,cluster){

        if(myLayer.hasLayer(cluster)){
            let truc=myLayer.getLayerId(cluster);
             myLayer.removeLayer(truc);
         };
    }


    function setMarkers(array,greenIcon,cluster,bunds){

        for (let i = 0; i < array[0].length; i++) {
            let popup = L.popup({
                closeButton:true,
                className:"popupStyle"
                
            }).setContent(`<h3>${array[0][i].Titre}</h3> <h6>${array[0][i].Com}</h6><p>${array[0][i].Desc}</p>`);
            
            if (bunds.contains([array[0][i].Lat, array[0][i].Lng])) {
                 let marker = new L.marker([array[0][i].Lat,array[0][i].Lng], {icon: greenIcon}).bindPopup(popup);
                cluster.addLayer(marker);
            }
          }
    }


    // export function createArea(){
//     fetch("https://data.opendatasoft.com/api/records/1.0/search/?dataset=european-union-countries%40public&q=France&facet=sovereignt")
//     .then(function(reponse){
//             reponse.json().then(function(geoFrance){
//             let france = geoFrance.records[0].fields.geo_shape;
//             poly = L.geoJSON(france).addTo(map);
//         })
//     })
// }